// #include functions like import in python
#include <stdio.h> // stdio.h is the standard bulk C Library
#include <stdbool.h> // boolean library

int makinWhiles() {
    // while lets you repeat code forever or until something is met
    int a = 0;
    while (a < 5) {
        printf("a is equal to %d\n", a);
        a++;
    }

    // do/while is another option
    // the code will be executed always at least once
    // before the condition is test, i.e is 5 equal to a
    do {
        printf("a is equal to %d\n", a);
        a++;
    }
    while ( a < 10);

    // now on to for loops
    // statement 1 is b = 0
    // statement 2 is b < 5
    // statement 3 is b++
    // must declare variable first even if empty
    int b, c;
    // outer loop
    for (b = 0; b < 5; b++) {
        printf("b is now %d\n", b); // executes 5 times

        // inner loop
        for (c = 0; c < 2; c++) {
            printf("c is now %d\n", c); // executes 10 times (5*2)
        }
    }

    return 0;
}

int switchin() {
    // Example
    // looks for a matching block (day is equal to 4)
    // and executes code within that block
    // better than using multiple if else statements
    // the "break;" key as well stops checking cases
    // within the code blocks and ignores the rest

    int day = 4;

    switch (day) {
    case 1:
        printf("Monday\n");
        break;
    case 2:
        printf("Tuesday\n");
        break;
    case 3:
        printf("Wednesday\n");
        break;
    case 4:
        printf("Thursday\n");
        break;
    case 5:
        printf("Friday\n");
        break;
    case 6:
        printf("Saturday\n");
        break;
    case 7:
        printf("Sunday\n");
        break;
    }
    
    makinWhiles();

    return 0;
}

int bowl() {
    printf("\n\nSTARTING BOOL NOW\n");

    // true or false, 1 or 0, whatever it is
    bool isGabyCute = true;
    bool isMichCute = false;

    // boolean values are returned as integers, you need %d
    printf("%d\n", isGabyCute);
    printf("%d\n", isMichCute);

    // comparing values, 1 is true, 0 is false
    int x = 5;
    int y = 12;

    // is x greater than
    printf("%d\n", x > y);

    // is x equal to
    printf("%d\n", x == 5);

    // real world use
    int myAge = 23;
    int ageToVote = 18;

    // lets use if/else, is myAge greater than or equal to ageToVote
    if (myAge > ageToVote) {
        printf("You can vote!\n");
    } else if (myAge == ageToVote) {
        printf("You're exactly 18, you can vote\n");
    } else {
        printf("You can't vote...\n");
    }

    // can also be done like this
    (myAge >= ageToVote) ? printf("You can vote!\n") : printf("You can't vote...\n");

    switchin();

    return 0;
} 

int new() {
    printf("This is a new function\n");

    // defining an integer
    int myNum = 17;

    // defining float
    float myDec = 12.33;

    // defining letter
    char myChar = 'a';

    // printing these variables with specifiers - %d int - %f float - %c char
    printf("%d\n", myNum);
    printf("I have %f life left out of 100\n", myDec);
    printf("I have %c friend\n", myChar);

    // quick mafs
    float sum1 = myNum + myDec;
    printf("Sum1 is %.2f\n", sum1); // .2 in %.2f limits decimal to "2" digits after the point

    // multiple vars
    int x = 1, y = 19, z = 12;
    int sum2 = x + y * z / myNum;
    printf("Sum2 is %d\n", sum2);

    // constant vars cant be redefined
    const int v = 15;
    const int e = 12;
    // int v = 12; would return an error
    x += y; // adds y to x, redefining x with a new value
    x++; // adds 1
    x--; // subtracts 1
    printf("%d\n", x);

    bowl();

    return 0;
}

// use int to define and start a new function, here its called main
int main() { 
    // must have semicolons at the end of each line
    printf("Hello World\n");
    printf("I'm learning C.\n");

    // execs function
    new();

    // return 0 ends the function
    return 0;
} // curly braces help contain your function